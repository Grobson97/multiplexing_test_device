import gdspy
import numpy as np
import util.filter_bank_builder_tools as tools
from models.filter_bank import FilterBank
from models.lekid import Lekid


def make_single_spectrometer_cell(
    lekid_f0_array: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
):
    """
    Function to add a single spectrometer geometry with connected lekids to a desired cell with the connection to the
    filter-bank feedline at (0,0). This will return the updated spectrometer_cell and the two spectrometer readout
    connection coordinates, where readout_connection_1 is the upper side, and readout_connection_2 is the lower side.

    :param lekid_f0_array: Array of f0's for the LEKIDS in the spectrometer.
    :param filter_bank: Instance of the filter-bank to which the LEKIDS are connected.
    :param spectrometer_cell: Cell to add the spectrometer to.
    :param readout_width: Width of the CPW readout line.
    :param readout_gap: Gap of the CPW readout line.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return: spectrometer_cell, readout_connection_1, readout_connection_2
    """

    filter_bank_cell = filter_bank.make_cell(
        microstrip_layer=layers["niobium_microstrip"],
        ground_layer=layers["niobium_ground"],
        cell_name=spectrometer_cell.name + " Filter Bank",
    )

    spectrometer_cell.add(filter_bank_cell)

    lekid_connection_array = filter_bank.get_lekid_connections()

    # Find largest lekid in the spectrometer to use to set the total length of each lekid in spectrometer.
    max_idc_length = tools.lekid_idc_length(np.min(lekid_f0_array))
    max_coupler_length = tools.lekid_coupler_length(max_idc_length)

    readout_connections = [[], []]

    for count, connection in enumerate(lekid_connection_array):

        # Create instance of current connections lekid for each spectrometer:
        lekid = Lekid(
            target_f0=lekid_f0_array[count],
            signal_input_x=0.0,
            signal_input_y=0.0,
        )

        # Calculate length required to extend signal in connection so LEKID readout line is straight for spectrometer.
        extra_signal_in_length = (
            (max_idc_length + max_coupler_length)
            - lekid.idc_length
            - lekid.coupler_length
        )

        rotation_radians = 0
        list_index = 0  # variable to change which connection array the current lekid is added to. Allows two sub arrays
        # per spectrometer, one for the upper side and one the lower.
        if connection[2] == "down":
            rotation_radians = np.pi
            list_index = 1
            extra_signal_in_length = (
                -extra_signal_in_length
            )  # make length minus if lekid is down.
        rotation_degrees = (rotation_radians / (2 * np.pi)) * 360

        # Join filter channel output to LEKID inductor input:
        filter_to_inductor_path = gdspy.FlexPath(
            points=[
                (connection[0], connection[1]),
                (connection[0], connection[1] + extra_signal_in_length),
            ],
            width=3.0,
            layer=layers["niobium_microstrip"],
        )

        # Rotate coordinates of LEKID readout depending on which side of the FBS the LEKID is.
        lekid_readout_coordinates_input = tools.rotate_coordinates(
            lekid.get_readout_connections(
                readout_width=readout_width, readout_gap=readout_gap
            )[0][0],
            lekid.get_readout_connections(
                readout_width=readout_width, readout_gap=readout_gap
            )[0][1],
            angle=rotation_radians,
        )
        lekid_readout_coordinates_output = tools.rotate_coordinates(
            lekid.get_readout_connections(
                readout_width=readout_width, readout_gap=readout_gap
            )[1][0],
            lekid.get_readout_connections(
                readout_width=readout_width, readout_gap=readout_gap
            )[1][1],
            angle=rotation_radians,
        )

        # Add LEKID connections to readout list relative to the origin of the filter-bank.
        readout_connections[list_index].append(
            [
                [
                    0.0 + connection[0] + lekid_readout_coordinates_input[0],
                    0.0
                    + connection[1]
                    + extra_signal_in_length
                    + lekid_readout_coordinates_input[1],
                ],
                [
                    0.0 + connection[0] + lekid_readout_coordinates_output[0],
                    0.0
                    + connection[1]
                    + extra_signal_in_length
                    + lekid_readout_coordinates_output[1],
                ],
            ]
        )

        # Make cells for each lekid:
        lekid_cell = lekid.make_cell(
            back_etch_layer=layers["back_etch"],
            inductor_layer=layers["aluminium_microstrip"],
            idc_layer=layers["niobium_microstrip"],
            readout_layer=layers["niobium_microstrip"],
            ground_layer=layers["niobium_ground"],
            dielectric_layer=layers["sin"],
            readout_width=readout_width,
            readout_gap=readout_gap,
            cell_name=spectrometer_cell.name + " Lekid_" + str(count),
        )

        lekid_reference = gdspy.CellReference(
            lekid_cell,
            (connection[0], connection[1] + extra_signal_in_length),
            rotation=rotation_degrees,
        )

        spectrometer_cell.add([lekid_reference, filter_to_inductor_path])

    connect_spectrometer_readouts(
        readout_connections=readout_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_cell=spectrometer_cell,
        layers=layers,
    )

    spectrometer_readout_connection_1 = (
        readout_connections[0][0][0][0],
        readout_connections[0][0][0][1],
    )
    spectrometer_readout_connection_2 = (
        readout_connections[1][0][1][0],
        readout_connections[1][0][1][1],
    )

    return (
        spectrometer_cell,
        spectrometer_readout_connection_1,
        spectrometer_readout_connection_2,
    )


def make_quad_spectrometer_cell(
    spectrometer_a_origin: tuple,
    spectrometer_b_origin: tuple,
    spectrometer_c_origin: tuple,
    spectrometer_d_origin: tuple,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    lekid_f0_array_c: np.ndarray,
    lekid_f0_array_d: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_group_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
):
    """
        Function to add a quad spectrometer geometry (A group of 4 spectrometers, A, B, C, D stacked vertically, where
        A's origin is the highest in y and D's the lowest) to the desired spectrometer_group_cell. This will return the
        updated spectrometer_group_cell and the coordinates for the connection at the upper side of spectrometer A, and
        the bottom side of spectrometer D.

        :param spectrometer_a_origin: Origin of spectrometer A (The connection of the filter-bank feedline).
        :param spectrometer_b_origin: Origin of spectrometer B (The connection of the filter-bank feedline).
        :param spectrometer_c_origin: Origin of spectrometer C (The connection of the filter-bank feedline).
        :param spectrometer_d_origin: Origin of spectrometer D (The connection of the filter-bank feedline).
        :param lekid_f0_array_a:
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_c: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_d: Array of f0's for the LEKIDS in spectrometer B.
        :param filter_bank:
        :param spectrometer_group_cell: Cell to add the four spectrometers to.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    }
        :return: spectrometer_group_cell, readout_connection_1_a, readout_connection_d_2
    """

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell("Spectrometer A")
    spectrometer_cell_b = gdspy.Cell("Spectrometer B")
    spectrometer_cell_c = gdspy.Cell("Spectrometer C")
    spectrometer_cell_d = gdspy.Cell("Spectrometer D")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_c,
        readout_connection_1_c,
        readout_connection_2_c,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_c,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_c,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_d,
        readout_connection_1_d,
        readout_connection_2_d,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_d,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_d,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        spectrometer_cell_a, spectrometer_a_origin
    )
    spectrometer_reference_b = gdspy.CellReference(
        spectrometer_cell_b, spectrometer_b_origin
    )
    spectrometer_reference_c = gdspy.CellReference(
        spectrometer_cell_c, spectrometer_c_origin
    )
    spectrometer_reference_d = gdspy.CellReference(
        spectrometer_cell_d, spectrometer_d_origin
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
            spectrometer_reference_c,
            spectrometer_reference_d,
        ]
    )
    spectrometer_connections = np.array(
        [
            np.array(readout_connection_1_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_2_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_1_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_2_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_1_c) + np.array(spectrometer_c_origin),
            np.array(readout_connection_2_c) + np.array(spectrometer_c_origin),
            np.array(readout_connection_1_d) + np.array(spectrometer_d_origin),
            np.array(readout_connection_2_d) + np.array(spectrometer_d_origin),
        ]
    )

    connect_spectrometers(
        spectrometer_connections=spectrometer_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_group_cell=spectrometer_group_cell,
        layers=layers,
    )

    return (
        spectrometer_group_cell,
        spectrometer_connections[0],
        spectrometer_connections[-1],
    )


def connect_spectrometer_readouts(
    readout_connections: list,
    readout_width: float,
    readout_gap: float,
    spectrometer_cell: gdspy.Cell,
    layers: dict,
):
    """
    Function to connect all the LEKID readouts together in a spectrometer. Readout geometry is added to the spectrometer
    cell. Does not return anything.

    :param readout_connections: Array of lekid readout connections, must have the format:
    [Upper/lower][LEKID][input/output][x/y].
    :param readout_width: Width of readout CPW transmission line.
    :param readout_gap: Gap size of readout CPW transmission line.
    :param spectrometer_cell: Spectrometer cell to add the connections to.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return:
    """
    # Section to connect all the Lekid readout connections together.

    for count, connection in enumerate(readout_connections[0]):
        if connection != readout_connections[0][-1]:
            tools.cpw_connect_parallel_lines(
                x1=connection[1][0],
                y1=connection[1][1],
                x2=readout_connections[0][count + 1][0][0],
                y2=readout_connections[0][count + 1][0][1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=readout_width * 3,
                centre_layer=layers["niobium_microstrip"],
                ground_layer=layers["niobium_ground"],
                target_cell=spectrometer_cell,
                parallel_in_x=True,
            )
    for count, connection in enumerate(readout_connections[1]):
        if connection != readout_connections[1][-1]:
            tools.cpw_connect_parallel_lines(
                x1=connection[0][0],
                y1=connection[0][1],
                x2=readout_connections[1][count + 1][1][0],
                y2=readout_connections[1][count + 1][1][1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=readout_width * 3,
                centre_layer=layers["niobium_microstrip"],
                ground_layer=layers["niobium_ground"],
                target_cell=spectrometer_cell,
                parallel_in_x=True,
            )

    # Connecting final lekid on upper row of spectrometer to final lekid on the lower row.
    tools.cpw_connect_points(
        x1=readout_connections[0][-1][1][0],
        y1=readout_connections[0][-1][1][1],
        x2=readout_connections[0][-1][1][0] + 1000,
        y2=0.0,
        centre_width=readout_width,
        ground_gap=readout_gap,
        bend_radius=200,
        centre_layer=layers["niobium_microstrip"],
        ground_layer=layers["niobium_ground"],
        target_cell=spectrometer_cell,
        x_first=True,
    )
    tools.cpw_connect_points(
        x1=readout_connections[0][-1][1][0] + 1000,
        y1=0.0,
        x2=readout_connections[1][-1][0][0],
        y2=readout_connections[1][-1][0][1],
        centre_width=readout_width,
        ground_gap=readout_gap,
        bend_radius=200,
        centre_layer=layers["niobium_microstrip"],
        ground_layer=layers["niobium_ground"],
        target_cell=spectrometer_cell,
        x_first=False,
    )


def connect_spectrometers(
    spectrometer_connections: np.ndarray,
    readout_width: float,
    readout_gap: float,
    spectrometer_group_cell: gdspy.Cell,
    layers: dict,
):
    """
    Function to connect the cpw readout lines of vertically stacked spectrometers.

    :param spectrometer_connections: Array of spectrometer connection coordinates in the format: [input_A, output_A,
    input_B, output_B, input_C, output_C, input_D, output_D...]
    :param readout_width: Width of readout CPW transmission line.
    :param readout_gap: Gap size of readout CPW transmission line.
    :param spectrometer_group_cell:  Spectrometer group cell to add the connections to.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return:
    """

    for count, connection in enumerate(spectrometer_connections):
        if count != 0 and tools.is_even(count):
            tools.cpw_connect_points(
                x1=spectrometer_connections[count - 1][0],
                y1=spectrometer_connections[count - 1][1],
                x2=spectrometer_connections[count - 1][0] - 1000,
                y2=(spectrometer_connections[count - 1][1] + connection[1]) / 2,
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=200,
                centre_layer=layers["niobium_microstrip"],
                ground_layer=layers["niobium_ground"],
                target_cell=spectrometer_group_cell,
                x_first=True,
            )
            tools.cpw_connect_points(
                x1=spectrometer_connections[count - 1][0] - 1000,
                y1=(spectrometer_connections[count - 1][1] + connection[1]) / 2,
                x2=connection[0],
                y2=connection[1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=200,
                centre_layer=layers["niobium_microstrip"],
                ground_layer=layers["niobium_ground"],
                target_cell=spectrometer_group_cell,
                x_first=False,
            )
