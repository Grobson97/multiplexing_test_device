import gdspy


def import_gds(file_path: str, cell_name=None):
    """
    Imports a GDS file and returns a cell with all the corresponding geometry polygons
    ----------
    :param file_path: Str, Path or name of file to be imported
    :param cell_name: Str, Name of the cell that will be returned as a Device.  If None, will automatically select the
    topmost cell.
    """

    imported_library = gdspy.GdsLibrary()
    imported_library.read_gds(file_path, units="convert")
    top_level_cells = imported_library.top_level()
    if len(top_level_cells) == 1:
        top_cell = top_level_cells[0]
    elif len(top_level_cells) > 1:
        raise ValueError(
            "import_gds() There are multiple top-level "
            "cells, you must specify `cell_name` to select of "
            "one of them"
        )
    top_cell.name=cell_name
    return top_cell
