import gdspy
import numpy as np
import math


def is_even(number: int):
    """
    Function to return True or false depending on whether the integer number is even or not.

    :param number:
    :return:
    """
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


def microstrip_connect_points(
    x1: float,
    y1: float,
    x2: float,
    y2: float,
    microstrip_width: float,
    bend_radius: float,
    microstrip_layer: int,
    target_cell: gdspy.Cell,
    x_first: bool,
) -> None:
    """
    Function to add a microstrip path between two points, (x1,y1) and (x2,y2).
    The microstrip path geometry is then added to the target_cell. The order of
    which axis is traversed first is defined by parallel_in_x. e.g. If True the path moves
    along the x-axis and then y.

    :param x1: x coordinate of first point.
    :param y1: y coordinate of first point.
    :param x2: x coordinate of second point.
    :param y2: y coordinate of second point.
    :param microstrip_width: Width of microstrip path.
    :param bend_radius: Radius of the 90 degree turn.
    :param microstrip_layer: GDSII layer for microstrip.
    :param target_cell: The cell which the geometry will be added to.
    :param x_first: Boolean to determine which axis the path traverses
    first.
    """

    mid_point = (x2, y1)
    if not x_first:
        mid_point = (x1, y2)

    path = gdspy.FlexPath(
        points=[(x1, y1), mid_point, (x2, y2)],
        width=microstrip_width,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        layer=microstrip_layer,
    )

    target_cell.add(path)


def cpw_connect_points(
    x1: float,
    y1: float,
    x2: float,
    y2: float,
    centre_width: float,
    ground_gap: float,
    bend_radius: float,
    centre_layer: int,
    ground_layer: int,
    target_cell: gdspy.Cell,
    x_first: bool,
    add_bridges=True,
) -> None:
    """
    Function to add a CPW path between two points, (x1,y1) and (x2,y2).
    The CPW path geometry is then added to the target_cell. The order of
    which axis is traversed first is defined by parallel_in_x. e.g. If True the path moves
    along the x-axis and then y.

    :param centre_layer:
    :param x1: x coordinate of first point.
    :param y1: y coordinate of first point.
    :param x2: x coordinate of second point.
    :param y2: y coordinate of second point.
    :param centre_width: Width of CPW central path.
    :param ground_gap: Gap width between centre and ground plane.
    :param bend_radius: Radius of the 90 degree turn.
    :param centre_layer: GDSII layer for CPW center.
    :param ground_layer: GDSII layer for ground plane.
    :param target_cell: The cell which the geometry will be added to.
    :param x_first: Boolean to determine which axis the path traverses
    :param add_bridges: Boolean to add bridges using the bridge_builder function.
    first.
    """

    mid_point = (x2, y1)
    if not x_first:
        mid_point = (x1, y2)

    centre_path = gdspy.FlexPath(
        points=[(x1, y1), mid_point, (x2, y2)],
        width=centre_width,
        offset=0.0,
        corners="circular bend",
        bend_radius=200,
        max_points=1999,
        layer=centre_layer,
    )

    gap_path = gdspy.FlexPath(
        points=[(x1, y1), mid_point, (x2, y2)],
        width=centre_width + 2 * ground_gap,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        max_points=1999,
        layer=ground_layer,
    )

    if add_bridges:
        bridge_id = target_cell.name + " %i%i" % (
            x1,
            y1,
        )  # Unique ID made from the starting coordinate of the line.
        bridges = bridge_builder(
            path_points=[(x1, y1), mid_point, (x2, y2)],
            bridge_width=centre_width + 2 * ground_gap,
            bridge_thickness=6.0,
            bridge_separation=500.0,
            bridge_layer=ground_layer,
            bridge_cell_id=bridge_id,
        )
        gap_path = gdspy.boolean(
            operand1=gap_path,
            operand2=bridges,
            operation="not",
            layer=ground_layer,
        )

    target_cell.add([centre_path, gap_path])


def cpw_connect_parallel_lines(
    x1: float,
    y1: float,
    x2: float,
    y2: float,
    centre_width: float,
    ground_gap: float,
    bend_radius: float,
    centre_layer: int,
    ground_layer: int,
    target_cell: gdspy.Cell,
    parallel_in_x: bool,
) -> None:
    """
    Function to add a CPW path between two points on parallel lines via a flat, diagonal then flat line connecting
    points (x1,y1) and (x2,y2). The CPW path geometry is then added to the target_cell. The order of which axis is
    traversed first is defined by x_first. e.g. If True the path moves along the x-axis and then y.

    :param centre_layer:
    :param x1: x coordinate of first point.
    :param y1: y coordinate of first point.
    :param x2: x coordinate of second point.
    :param y2: y coordinate of second point.
    :param centre_width: Width of CPW central path.
    :param ground_gap: Gap width between centre and ground plane.
    :param bend_radius: Radius of the 90 degree turn.
    :param centre_layer: GDSII layer for CPW center.
    :param ground_layer: GDSII layer for ground plane.
    :param target_cell: The cell which the geometry will be added to.
    :param parallel_in_x: Boolean to determine which axis the path traverses
    first.
    """

    mid_point = ((x1 + x2) / 2, (y1 + y2) / 2)

    dx = 0
    dy = centre_width
    if parallel_in_x:
        dx = centre_width
        dy = 0

    path = gdspy.FlexPath(
        points=[(x1, y1), (x1 + dx, y1 + dy), mid_point, (x2 - dx, y2 - dy), (x2, y2)],
        width=[centre_width, centre_width + 2 * ground_gap],
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        layer=[centre_layer, ground_layer],
    )

    target_cell.add(path)


def create_target_f0_array(
    f_min: float, f_max: float, number_of_channels: int
) -> np.ndarray:
    """Returns a numpy array of log spaced target f0 values between the bounds of the frequency band.

    :param f_min: minimum frequency of the band
    :param f_max: maximum frequency of the band
    :param number_of_channels: number of channels in the filter, should be determined from resolving power and
    oversampling factor.
    :returns: A numpy array of log spaced target f0 values between the bounds of the frequency band.

    """
    target_f0_array = []
    fr = f_max
    while len(target_f0_array) < number_of_channels:
        target_f0_array.append(fr)
        fr = fr * np.exp(-(np.log(f_max) - np.log(f_min)) / (number_of_channels - 1))

    return np.array(target_f0_array)


def rotate_coordinates(x: float, y: float, angle: float):
    """
    Function to rotate 2d coordinates (x and y, by a given angle about the origin.

    :param x: x coordinate to rotate.
    :param y: y coordinate to rotate.
    :param angle: angle of rotation in radians
    :return: new_omt_x_inner, new_omt_y_inner, rotated coordinates.
    """
    new_x = x * np.cos(angle) - y * np.sin(angle)
    new_y = x * np.sin(angle) + y * np.cos(angle)

    return new_x, new_y


def bridge_builder(
    path_points: list,
    bridge_width: float,
    bridge_thickness: float,
    bridge_separation: float,
    bridge_layer: int,
    bridge_cell_id="",
) -> gdspy.Cell:
    """
    Function to build bridges along the path of a CPW path, connecting the ground planes.
    Returns the cell of the bridges.

    :param path_points: List of CPW path points.
    :param bridge_width: Width of the bridges, equivalent to the total CPW
    ground plane gap width.
    :param bridge_thickness: Thickness of the bridge, dimension parallel
    to path direction.
    :param bridge_separation: Distance between adjacent bridges.
    :param bridge_layer: GDSII layer for the bridges, should be the same
    as the ground planes.
    :param bridge_cell_id: String identifier to give bridge cell a unique name. Avoids multiples of the same cell being
    added to library.
    """

    bridge_cell = gdspy.Cell("Bridge Cell " + bridge_cell_id)
    bridges_cell = gdspy.Cell("Bridges Cell " + bridge_cell_id)

    bridge_cell.add(
        gdspy.Rectangle(
            point1=(-bridge_thickness / 2, bridge_width / 2),
            point2=(bridge_thickness / 2, -bridge_width / 2),
            layer=bridge_layer,
        )
    )

    for count, point in enumerate(path_points):

        # If to avoid going out of index at final point.
        if point != path_points[-1]:

            bridge_section_cell = gdspy.Cell(
                "Bridge Section " + bridge_cell_id + str(count)
            )

            end_point = path_points[count + 1]

            x1 = point[0]
            y1 = point[1]
            x2 = end_point[0]
            y2 = end_point[1]

            # Block to calculate required rotation angle of the bridge:
            vector = [x2 - x1, y2 - y1]
            unit_vector = vector / np.linalg.norm(vector)
            x_unit_vector = [1.0, 0.0]
            dot_product = np.dot(unit_vector, x_unit_vector)
            cross_product = np.cross(x_unit_vector, vector)
            angle = np.arccos(dot_product) / np.pi * 180
            # If cross product is less than zero, angle is counter clockwise, hence correction:
            if cross_product < 0:
                angle = 360 - angle

            # Block to place bridges centrally between both points at a fixed distance:
            distance = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            number_of_bridges = math.floor(distance / bridge_separation)
            bridge_x_positions = np.linspace(
                start=1, stop=number_of_bridges, num=number_of_bridges
            )
            bridge_x_positions = (
                bridge_x_positions - np.sum(bridge_x_positions) / number_of_bridges
            )
            bridge_x_positions = distance / 2 + bridge_separation * bridge_x_positions

            # Loop to place bridges along the x axis at the required positions and spacings.
            for position in bridge_x_positions:
                bridge_cell_reference = gdspy.CellReference(
                    ref_cell=bridge_cell, origin=(position, 0.0), rotation=0.0
                )
                bridge_section_cell.add(bridge_cell_reference)

            # Create reference cell of bridge section and rotate to required angle.
            bridge_section_ref_cell = gdspy.CellReference(
                ref_cell=bridge_section_cell, origin=point, rotation=angle
            )
            bridges_cell.add(bridge_section_ref_cell)

    return bridges_cell


def make_quad_mixed_array(
    start_f0_a: float,
    start_f0_b: float,
    start_f0_c: float,
    start_f0_d: float,
    f0_spacing: float,
    number_of_channels: int,
) -> np.ndarray:
    """
    Function to build an array of 4 sub-arrays (a, b, c, d) mixed together such that the array becomes:
    [a0, b0, c0, d0, a1, b1, c1, d1, ..... an, bn, cn, dn] until the number of channels is met.

    :param start_f0_a:
    :param start_f0_b:
    :param start_f0_c:
    :param start_f0_d:
    :param f0_spacing:
    :param number_of_channels:
    :return:
    """

    f0_array = []

    f0_a = start_f0_a
    f0_b = start_f0_b
    f0_c = start_f0_c
    f0_d = start_f0_d

    sequence = 0
    a_count = 0
    b_count = 0
    c_count = 0
    d_count = 0
    for n in range(number_of_channels):
        if sequence == 0:
            f0_array.append(f0_a)
            f0_a += f0_spacing
            a_count += 1
            sequence = 1
            continue
        if sequence == 1:
            f0_array.append(f0_b)
            f0_b += f0_spacing
            b_count += 1
            sequence = 2
            continue
        if sequence == 2:
            f0_array.append(f0_c)
            f0_c += f0_spacing
            c_count += 1
            sequence = 3
            continue
        if sequence == 3:
            f0_array.append(f0_d)
            f0_d += f0_spacing
            d_count += 1
            sequence = 0

    print(
        "Total f0's per sub-array: a - %i, b - %i, c - %i, d - %i"
        % (a_count, b_count, c_count, d_count)
    )
    print(
        "Sub-array gap: %0.4e Hz"
        % (start_f0_b - (start_f0_a + (a_count - 1) * f0_spacing))
    )

    return np.array(f0_array)


def mix_array(array: np.ndarray) -> np.ndarray:
    """
    Function that takes an array and mixes it such that the original array
    ["L1", "L2", "L3", "L4",..., "H1", "H2", "H3", "H4",...], becomes ['L1' 'L2' 'H1' 'H2' 'L3' 'L4' 'H3' 'H4',...].

    :param array: Array to mix
    :return: mixed array.
    """

    remainder = array.size % 4
    if remainder != 0:
        split = np.split(array, [-remainder])
        array = split[0]
        excess = split[1]

    split_array = np.split(
        array, 2
    )  # ['L1', 'L2', 'L3', 'L4'] and ['H1', 'H2', 'H3', 'H4']
    l_array_odd = split_array[0][::2]  # ['L1', 'L3',...]
    l_array_even = split_array[0][1::2]  # ['L2', 'L4',...]
    h_array_odd = split_array[1][::2]  # ['H1', 'H3',...]
    h_array_even = split_array[1][1::2]  # ['H2', 'H4',...]
    l_array = np.dstack(
        (l_array_odd, l_array_even)
    )  # [[['L1' 'L2'], ['L3', 'L4'], ...]]
    h_array = np.dstack(
        (h_array_odd, h_array_even)
    )  # [[['H1' 'H2'], ['H3', 'H4'], ...]]
    mixed_array = np.dstack((l_array, h_array)).flatten()

    if remainder != 0:
        mixed_array = np.hstack((mixed_array, excess))

    return mixed_array


def lekid_idc_length(target_f0):
    """
    Function to calculate the length of the IDC for a hybrid LEKID at a given f0.

    :param target_f0: F0 of LEKID resonator.
    :return:
    """

    return (
        -2.558e-25 * target_f0**3
        + 2.237e-15 * target_f0**2
        - 7.461e-06 * target_f0
        + 9096
    )


def lekid_coupler_length(idc_length):
    """
    Function to calculate the length of the coupler for a hybrid sparse_lekid at a given IDC length.

    :param idc_length: Length of IDC.
    :return:
    """
    return -1.083e-5 * idc_length**2 + 0.06206 * idc_length + 93.47
