import gdspy
import numpy as np
import util.filter_bank_builder_tools as tools
from models.single_filter import SingleFilter


class FilterBank:
    def __init__(
        self,
        target_f0_array: np.array,
        feedline_width: float,
        filter_separation: float,
        input_connection_x=0.0,
        input_connection_y=0.0,
    ) -> None:
        """
        Creates a new instance of a filter bank with filters at the target
        frequencies defined in target_f0_array spaced lambda/4 apart.


        :param: target_f0: Target resonant frequency in GHz for the filter.
        :param: feedline_width: Width of filterbank feedline in um.
        :param: filter_separation: Defines the mm-wave spacing each filter is separated by. e.g. 0.25 = quarter wave
        spaced.
        :param: input_connection_x: x-coordinate of the connection to the
        feedline from the antenna.
        :param: input_connection_y: y-coordinate of the connection to the
        feedline from the antenna.
        """

        self.target_f0_array = target_f0_array
        self.feedline_width = feedline_width
        self.filter_separation = filter_separation
        self.input_connection_x = input_connection_x
        self.input_connection_y = input_connection_y

    def make_cell(
        self, microstrip_layer: int, ground_layer: int, cell_name="Filter Bank"
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given filter-bank geometry.
        The geometry is oriented such that coordinates of the connection to the
        feedline has the smallest x value and the feedline extends in the +ve x
        direction with the filter connected parallel to the y axis.

        :param microstrip_layer: GDSII layer for microstrip geometry.
        :param ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param cell_name: Name to give to the gdspy.Cell.
        """

        # Create Lekid cell to add module cells to
        filter_bank_cell = gdspy.Cell(cell_name)

        # Create feedline path:
        feedline_path = gdspy.Path(
            width=self.feedline_width,
            initial_point=(self.input_connection_x, self.input_connection_y),
        )

        # Add a small segment to the feedline from the input point going in the
        # '+x' direction:
        initial_segment_length = 11
        feedline_path.segment(
            length=initial_segment_length, direction="+x", layer=microstrip_layer
        )

        # Define connection coordinates and rotation of first filter:
        filter_connection_x = self.input_connection_x + initial_segment_length
        filter_connection_y = self.input_connection_y - self.feedline_width / 2
        rotation = 0.0

        # For loop to build all filters at each target_f0:
        for count, target_f0 in enumerate(self.target_f0_array):
            if target_f0 != self.target_f0_array[-1]:
                # Create single filter at arbitrary location:
                single_filter = SingleFilter(
                    target_f0=target_f0,
                    feedline_connection_x=0.0,
                    feedline_connection_y=0.0,
                )

                single_filter_cell = single_filter.make_cell(
                    microstrip_layer=microstrip_layer,
                    ground_layer=ground_layer,
                    cell_name=cell_name + " Single Filter_" + str(count),
                )
                # Create reference cell of filter in correct location and orientation.
                single_filter_ref_cell = gdspy.CellReference(
                    single_filter_cell,
                    origin=(filter_connection_x, filter_connection_y),
                    rotation=rotation,
                )

                # Add filter geometry to filter_bank_cell.
                filter_bank_cell.add(single_filter_ref_cell)

                # Add interconnection between filters unless its the last filter:
                interconnection_length = self.filter_separation * 2 * single_filter.res_length

                if count != len(self.target_f0_array) - 2:
                    feedline_path.segment(
                        length=interconnection_length,
                        direction="+x",
                        layer=microstrip_layer,
                    )

                # If second to last filter add connection directly to Lekid location, skipping final filter.
                if count == len(self.target_f0_array) - 2:
                    feedline_path.segment(
                        length=interconnection_length - 3.0 * 3,
                        direction="+x",
                        layer=microstrip_layer,
                        final_width=3.0,
                    )
                    if tools.is_even(count):
                        feedline_path.turn(
                            radius=3.0 * 3,
                            angle="l",
                            layer=microstrip_layer,
                        )
                        feedline_path.segment(
                            length=self.feedline_width / 2 + 83.5 - (3.0 * 3),
                            direction="+y",
                            layer=microstrip_layer,
                        )

                    if not tools.is_even(count):
                        feedline_path.turn(
                            radius=3.0 * 3,
                            angle="r",
                            layer=microstrip_layer,
                        )
                        feedline_path.segment(
                            length=self.feedline_width / 2 + 83.5 - (3.0 * 3),
                            direction="-y",
                            layer=microstrip_layer,
                        )

                # Update filter connection location:
                filter_connection_x += interconnection_length

                # Allow for filters on either side of feedline:
                if tools.is_even(count):
                    filter_connection_y = (
                        self.input_connection_y + self.feedline_width / 2
                    )
                    rotation = 180.0
                if not tools.is_even(count):
                    filter_connection_y = (
                        self.input_connection_y - self.feedline_width / 2
                    )
                    rotation = 0.0

        filter_bank_cell.add(feedline_path)

        return filter_bank_cell

    def get_lekid_connections(self) -> list:
        """
        Function to return a list of coordinates for each connection point
        between the output of a filter and the input of a Lekid for a given
        filter-bank instance. The output format is (x, y, "up" or "down) where
        up or down indicates a filter below the feedline or above.
        """
        # Define empty array of connections to be filled:
        connections = []

        # Define connection coordinates and direction of first filter:
        initial_segment_length = 11.0
        lekid_connection_x = self.input_connection_x + initial_segment_length
        lekid_connection_y = self.input_connection_y - self.feedline_width / 2 - 83.5

        # Add to connections list:
        connections.append((lekid_connection_x, lekid_connection_y, "down"))

        # For loop for each filter:
        for count, target_f0 in enumerate(self.target_f0_array):

            resW = (
                -5.69e-5 * target_f0**3
                + 0.033 * target_f0**2
                - 7.13 * target_f0
                + 669
            )
            res_length = 12 + 2 * resW

            interconnection_length = self.filter_separation * 2 * res_length

            # Update filter connection location:
            lekid_connection_x += interconnection_length

            if tools.is_even(count):
                lekid_connection_y = (
                    self.input_connection_y + self.feedline_width / 2 + 83.5
                )
                direction = "up"  # NB: Updates location of the next filter not the current loop.
            if not tools.is_even(count):
                lekid_connection_y = (
                    self.input_connection_y - self.feedline_width / 2 - 83.5
                )
                direction = "down"  # NB: Updates location of the next filter not the current loop.

            if target_f0 != self.target_f0_array[-1]:
                connections.append((lekid_connection_x, lekid_connection_y, direction))
        return connections
