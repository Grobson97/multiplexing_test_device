import gdspy
from models.lekid_coupler import LekidCoupler
from models.idc import IDC
from models.inductor import Inductor


class Lekid:
    def __init__(
        self,
        target_f0: float,
        signal_input_x=0.0,
        signal_input_y=0.0,
    ) -> None:

        """
        Creates new instance of a single Lekid with an interdigital capacitor
        using the modules for the coupler, inductor and idc.

        :param float target_f0: Target resonant frequency in Hz for the Lekid.
        :param signal_input_x: X-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        :param signal_input_y: Y-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        """

        self.target_f0 = target_f0

        # NB: If the following interpolation models change you also need to change util.lekid_coupler_length() and
        # util.lekid_idc_length()
        self.idc_length = (
            -2.558e-25 * target_f0**3
            + 2.237e-15 * target_f0**2
            - 7.461e-06 * target_f0
            + 9096
        )

        self.coupler_length = (
            -1.083e-5 * self.idc_length**2 + 0.06206 * self.idc_length + 93.47
        )

        self.signal_input_x = signal_input_x
        self.signal_input_y = signal_input_y

        self.inductor = Inductor(
            track_width=2.0,
            bend_radius=6.0,
            signal_input_connection_x=signal_input_x,
            signal_input_connection_y=signal_input_y,
            tolerance=0.01,
        )

        self.idc = IDC(
            finger_width=3.0,
            finger_gap=6.0,
            horizontal_finger_length=143.0,
            vertical_finger_length=self.idc_length,
            inductor_connection_x=self.inductor.get_idc_connection_x(),
            inductor_connection_y=self.inductor.get_idc_connection_y(),
        )

        self.lekid_coupler = LekidCoupler(
            coupler_length=self.coupler_length,
            finger_width=3.0,
            finger_gap=8.0,
            readout_track_width=6.0,
            idc_track_width=6.0,
            length_to_idc=30.0,
            length_to_readout=82,
            idc_connection_x=self.idc.get_coupler_connection_x(),
            idc_connection_y=self.idc.get_coupler_connection_y(),
        )

    def make_cell(
        self,
        back_etch_layer: int,
        inductor_layer: int,
        idc_layer: int,
        readout_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        readout_width: float,
        readout_gap: float,
        cell_name="Lekid",
    ) -> gdspy.Cell:

        """
        Returns the gdspy Cell for a given Lekid geometry.
        The geometry is oriented such that coordinates of the connection to the
        readout line has the largest y value and the connection to the signal input
        the lowest.

        :param back_etch_layer: GDSII layer for back etch geometry.
        :param inductor_layer: GDSII layer for inductor geometry.
        :param idc_layer: GDSII layer for IDC geometry.
        :param readout_layer: GDSII layer for readout line geometry.
        :param ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param dielectric_layer: GDSII layer for the dielectric layer.
        :param readout_width: Width of CPW readout center line.
        :param readout_gap: CPW gap width for readout line.
        :param cell_name: Name to be used to reference the cell. If creating
        multiple differing LEKIDS give cell names as LEKID_N where N is a unique
        number.
        """

        # Create Lekid cell to add module cells to.
        lekid_cell = gdspy.Cell(cell_name)

        cell_id = "0"
        if cell_name != "Lekid":
            cell_id = cell_name.split("_")[1]

        # Make gdspy cells:
        inductor_cell = self.inductor.make_cell(
            inductor_layer, cell_name=cell_name + " Inductor_" + cell_id
        )
        idc_cell = self.idc.make_cell(
            back_etch_layer=back_etch_layer,
            idc_layer=idc_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            cell_name=cell_name + " IDC_" + cell_id,
        )
        lekid_coupler_cell = self.lekid_coupler.make_cell(
            readout_line_layer=readout_layer,
            back_etch_layer=back_etch_layer,
            idc_layer=idc_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            cell_name=cell_name + " Lekid_Coupler_" + cell_id,
        )

        # Create reference cells of each sub-structure.
        inductor_ref_cell = gdspy.CellReference(inductor_cell, (0, 0))
        idc_ref_cell = gdspy.CellReference(idc_cell, (0, 0))
        elbow_coupler_ref_cell = gdspy.CellReference(lekid_coupler_cell, (0, 0))

        # Add pull back in ground plane over the inductor to idc and feedline to coupler connections:
        pull_back1 = gdspy.Rectangle(
            (
                self.lekid_coupler.get_readout_connection_x() - 30,
                self.lekid_coupler.get_readout_connection_y() - 54,
            ),
            (
                self.lekid_coupler.get_readout_connection_x() + 30,
                self.lekid_coupler.get_readout_connection_y() - 44,
            ),
            layer=ground_layer,
        )
        pull_back2 = gdspy.Rectangle(
            (
                self.inductor.get_idc_connection_x() - 30,
                self.inductor.get_idc_connection_y() - 23,
            ),
            (
                self.inductor.get_idc_connection_x() + 30,
                self.inductor.get_idc_connection_y() - 33,
            ),
            layer=ground_layer,
        )

        readout_path_points = [
            (
                self.lekid_coupler.get_readout_connection_x() - 80,
                self.lekid_coupler.get_readout_connection_y() + readout_width / 2,
            ),
            (
                self.lekid_coupler.get_readout_connection_x() + 60,
                self.lekid_coupler.get_readout_connection_y() + readout_width / 2,
            ),
        ]

        readout_centre_path = gdspy.FlexPath(
            points=readout_path_points,
            width=readout_width,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=readout_layer,
        )

        readout_gap_path = gdspy.FlexPath(
            points=readout_path_points,
            width=readout_width + 2 * readout_gap,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=ground_layer,
        )

        bridge = gdspy.Rectangle(
            (
                self.lekid_coupler.get_readout_connection_x() - 60,
                self.lekid_coupler.get_readout_connection_y()
                + readout_gap
                + readout_width,
            ),
            (
                self.lekid_coupler.get_readout_connection_x() - 54,
                self.lekid_coupler.get_readout_connection_y() - readout_gap,
            ),
            layer=10,
        )

        readout_gap_path = gdspy.boolean(
            operand1=readout_gap_path,
            operand2=bridge,
            operation="not",
            layer=ground_layer,
        )

        # Add reference cells to single_filter_cell.
        lekid_cell.add(
            [
                inductor_ref_cell,
                idc_ref_cell,
                elbow_coupler_ref_cell,
                pull_back1,
                pull_back2,
                readout_centre_path,
                readout_gap_path,
            ]
        )

        return lekid_cell

    def get_readout_connections(self, readout_width: float, readout_gap: float) -> list:
        """
        Function to return the coordinates of the two sparse_lekid readout connections in the form [(x1, y1), (x2, y2)]. Here
        x1 and y1 correspond to the LH connection, and x2 and y2 the RH connection in the default orientation.

        :param readout_width: Width of centre line of the readout CPW.
        :param readout_gap: Gap of the readout CPW.
        """

        x1 = self.lekid_coupler.get_readout_connection_x() - 80
        y = self.lekid_coupler.get_readout_connection_y() + readout_width / 2
        x2 = self.lekid_coupler.get_readout_connection_x() + 60

        return [(x1, y), (x2, y)]
