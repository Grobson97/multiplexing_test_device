import gdspy
from gdspy.library import Cell
import numpy as np


def is_even(number: int):
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


class IDC:
    def __init__(
        self,
        finger_width: float,
        finger_gap: float,
        horizontal_finger_length: float,
        vertical_finger_length: float,
        inductor_connection_x=0.0,
        inductor_connection_y=0.0,
    ) -> None:

        """
        Creates New instance of an IDC object.

        :param finger_width: Width of each IDC finger in microns.
        :param finger_gap: Gap size between IDC fingers in microns.
        :param horizontal_finger_length: Length of a horizontal finger in microns.
        :param vertical_finger_length: Length of a vertical (variable) finger in microns.
        :param inductor_connection_x: x coordinate of the IDC-inductor connection.
        :param inductor_connection_y: x coordinate of the IDC-inductor connection.
        """

        self.finger_width = finger_width
        self.finger_gap = finger_gap
        self.horizontal_finger_length = horizontal_finger_length
        self.vertical_finger_length = vertical_finger_length
        self.inductor_connection_x = inductor_connection_x
        self.inductor_connection_y = inductor_connection_y

    def make_cell(
        self,
        back_etch_layer: int,
        idc_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="IDC",
    ):

        """
        :param: int back_etch_layer: GDSII layer for back etch geometry.
        Returns the gdspy Cell for a given IDC instance. The geometry is
        oriented such that the IDC fingers are parallel to the x axis. To add a
        rotation this must be done with the .CellReference() method after
        calling this method.

        :param back_etch_layer: GDSII layer for back etch layer.
        :param idc_layer: GDSII layer for IDC geometry.
        :param ground_layer: GDSII layer for ground plane.
        :param dielectric_layer: GDSII layer for dielectric layer.
        :param cell_name: Name to be given to the cell.
        """

        origin_x = self.inductor_connection_x
        origin_y = self.inductor_connection_y
        finger_width = self.finger_width
        finger_gap = self.finger_gap
        horizontal_finger_length = self.horizontal_finger_length
        vertical_finger_length = self.vertical_finger_length

        rail_width = 6.0
        n_horizontal_fingers = 219
        n_vertical_fingers = 17

        # Create cell for complete IDC geometries to be added to.
        idc_cell = gdspy.Cell(cell_name)

        ################################################################################################################
        # Section to build horizontal fingers:

        horizontal_finger = gdspy.Rectangle(
            ((-horizontal_finger_length), (finger_width / 2)),
            (0.0, -(finger_width / 2)),
            layer=idc_layer,
        )
        horizontal_finger_cell = gdspy.Cell(name=cell_name + " H Finger")
        horizontal_finger_cell.add(horizontal_finger)

        for n in range(n_horizontal_fingers):
            dx = -finger_gap
            if is_even(n):
                dx = 0

            horizontal_finger_reference = gdspy.CellReference(
                ref_cell=horizontal_finger_cell,
                origin=(
                    origin_x + 4 + dx,
                    origin_y
                    + 13
                    + (finger_width / 2)
                    + n * finger_gap
                    + n * finger_width,
                ),
                rotation=0,
            )
            idc_cell.add(horizontal_finger_reference)

        ################################################################################################################
        # Section to build vertical fingers:
        vertical_finger = gdspy.Rectangle(
            (-vertical_finger_length, (finger_width / 2)),
            (0.0, -(finger_width / 2)),
            layer=idc_layer,
        )
        vertical_finger_cell = gdspy.Cell(name=cell_name + " V Finger")
        vertical_finger_cell.add(vertical_finger)

        for n in range(n_vertical_fingers):

            dy = +finger_gap
            if is_even(n):
                dy = 0

            vertical_finger_reference = gdspy.CellReference(
                ref_cell=vertical_finger_cell,
                origin=(
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width
                        + finger_width / 2
                        + n * finger_gap
                        + n * finger_width
                    ),
                    (
                        origin_y
                        + 13
                        + finger_width
                        + n_horizontal_fingers * finger_gap
                        + (n_horizontal_fingers - 1) * finger_width
                        + rail_width
                        + dy
                    ),
                ),
                rotation=-90,
            )
            idc_cell.add(vertical_finger_reference)

        ################################################################################################################
        # Section to add rails

        rail_path_right = gdspy.FlexPath(
            points=[
                (origin_x + 7, origin_y - 23),
                (
                    (origin_x + 7),
                    (
                        origin_y
                        + 13
                        + finger_width
                        + (n_horizontal_fingers + 1) * finger_gap
                        + (n_horizontal_fingers - 1) * finger_width
                        + 3 * rail_width / 2
                        + vertical_finger_length
                    ),
                ),
                (
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width / 2
                        - finger_width
                    ),
                    (
                        origin_y
                        + 13
                        + finger_width
                        + (n_horizontal_fingers + 1) * finger_gap
                        + (n_horizontal_fingers - 1) * finger_width
                        + 3 * rail_width / 2
                        + vertical_finger_length
                    ),
                ),
            ],
            width=rail_width,
            ends="flush",
            corners="natural",
            layer=idc_layer,
        )

        rail_path_left = gdspy.FlexPath(
            points=[
                (origin_x - 7, origin_y - 23),
                (origin_x - 7, origin_y),
                (
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width / 2
                    ),
                    origin_y,
                ),
                (
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width / 2
                    ),
                    origin_y
                    + 13
                    + finger_width
                    + (n_horizontal_fingers - 1) * finger_gap
                    + (n_horizontal_fingers - 1) * finger_width,
                ),
                (
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width / 2
                    ),
                    (
                        origin_y
                        + 13
                        + finger_width
                        + n_horizontal_fingers * finger_gap
                        + (n_horizontal_fingers - 1) * finger_width
                        + rail_width / 2
                    ),
                ),
                (
                    (
                        origin_x
                        + 4
                        - horizontal_finger_length
                        - finger_gap
                        - rail_width
                        + finger_width
                        + (n_vertical_fingers - 1) * finger_gap
                        + (n_vertical_fingers - 1) * finger_width
                    ),
                    (
                        origin_y
                        + 13
                        + finger_width
                        + n_horizontal_fingers * finger_gap
                        + (n_horizontal_fingers - 1) * finger_width
                        + rail_width / 2
                    ),
                ),
            ],
            width=rail_width,
            ends="flush",
            corners="natural",
            layer=idc_layer,
        )

        # Define vertices of the ground, dielectric and back_etch holes
        hole_vertex_1 = (
            (origin_x + 4 - horizontal_finger_length - finger_gap - rail_width - 30),
            (origin_y - 23),
        )
        hole_vertex_2 = (
            (origin_x + 4 + rail_width + 30),
            (
                origin_y
                + 13
                + finger_width
                + (n_horizontal_fingers + 1) * finger_gap
                + (n_horizontal_fingers - 1) * finger_width
                + vertical_finger_length
                + 2 * rail_width
            ),
        )

        # Add negative hole in ground layer:
        idc_gnd_hole = gdspy.Rectangle(
            hole_vertex_1,
            hole_vertex_2,
            layer=ground_layer,
        )
        # Add negative hole in dielectric layer:
        idc_dielectric_hole = gdspy.Rectangle(
            (hole_vertex_1[0] + 10, hole_vertex_1[1] + 10),
            (hole_vertex_2[0] - 10, hole_vertex_2[1]),
            layer=dielectric_layer,
        )

        # Add negative hole in back etch layer:
        back_etch_hole = gdspy.Rectangle(
            hole_vertex_1,
            hole_vertex_2,
            layer=back_etch_layer,
        )

        # Add remaining geometries to cell:
        idc_cell.add(
            [
                rail_path_right,
                rail_path_left,
                idc_gnd_hole,
                idc_dielectric_hole,
                back_etch_hole,
            ]
        )

        return idc_cell

    def get_coupler_connection_x(self):

        """
        Function to return the x-coordinate of IDC-Coupler join, located at the bottom furthest point on the RH rail
        away from the inductor connection.
        """

        origin_x = self.inductor_connection_x
        finger_gap = self.finger_gap
        horizontal_finger_length = self.horizontal_finger_length

        rail_width = 6.0

        return origin_x + 4 - horizontal_finger_length - finger_gap - rail_width / 2

    def get_coupler_connection_y(self):

        """
        Function to return the y-coordinate of IDC-Coupler join relative to the inductor connection, located at the
        bottom furthest point on the RH rail away from the inductor connection.
        """

        origin_y = self.inductor_connection_y
        finger_width = self.finger_width
        finger_gap = self.finger_gap
        vertical_finger_length = self.vertical_finger_length

        rail_width = 6.0
        n_horizontal_fingers = 219

        coupler_connection_y = (
            origin_y
            + 13
            + finger_width
            + (n_horizontal_fingers + 1) * finger_gap
            + (n_horizontal_fingers - 1) * finger_width
            + vertical_finger_length
            + 2 * rail_width
        )

        return coupler_connection_y
