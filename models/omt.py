import gdspy
import numpy as np


class OMT:
    def __init__(
        self,
        diameter: float,
        choke_width: float,
        dual_polarisation: True,
        center_x=0.0,
        center_y=0.0,
    ) -> None:

        """
        Creates a new instance of an orthomode transducer antenna. NB: All
        dimensions must be in micrometers.

        :param: diameter: Diameter of the OMT. The same diameter as the end of
        the feed horn.
        :param: choke_width: Distance between the inner and outer diameter of
        the choke.
        :param: dual_polarisation: Boolean, True means 2 pairs of antenna
        paddles. If false, only the 12 O'clock and 6 O'clock pair is built.
        :param: center_x: x-coordinate of the centre of the OMT.
        :param: center_y: x-coordinate of the centre of the OMT.
        """

        self.diameter = diameter
        self.choke_width = choke_width
        self.dual_polarisation = dual_polarisation
        self.center_x = center_x
        self.center_y = center_y

    def make_cell(
        self,
        antenna_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        back_etch_layer: int,
        cell_name="OMT",
        tolerance=0.01,
    ) -> gdspy.Cell:

        """
        Returns the gdspy Cell for a given OMT instance.

        :param: antenna_layer: GDSII layer for the antenna paddles.
        :param: ground_layer: GDSII layer for the ground plane.
        :param: dielectric_layer: GDSII layer for the dielectric.
        :param: back_etch_layer: GDSII layer for the etching of the bulk wafer.
        :param: cell_name: Name to be used to reference the cell.
        :param: tolerance: Approximate curvature resolution. The number of
        points is automatically calculated.
        """

        radius = self.diameter / 2
        center_x = self.center_x
        center_y = self.center_y

        omt_cell = gdspy.Cell(cell_name)

        # Make negative hole for ground layer:
        ground_hole = gdspy.Round(
            (center_x, center_y),
            radius=radius,
            tolerance=tolerance,
            layer=ground_layer,
        )

        # Make negative hole for dielectric layer:
        dielectric_hole = gdspy.Round(
            (center_x, center_y),
            radius=radius,
            tolerance=tolerance,
            layer=dielectric_layer,
        )

        # Make negative hole for back etch layer:
        back_etch_hole = gdspy.Round(
            (center_x, center_y),
            radius=radius + self.choke_width,
            tolerance=tolerance,
            layer=back_etch_layer,
        )

        probes = gdspy.Cell(cell_name + "Probes")

        probe_vertices = [
            (-135.0, 300.0),
            (-135.0, 900.0),
            (-1.0, 1200.0),
            (-1.0, 1205.0),
            (1.0, 1205.0),
            (1.0, 1200.0),
            (135.0, 900.0),
            (135.0, 300.0),
        ]

        probe_cell = gdspy.Cell(cell_name + "Probe")
        probe_cell.add(gdspy.Polygon(probe_vertices, layer=antenna_layer))

        # Add CPW to microstrip transition:
        cpw_to_microstrip_cell = gdspy.Cell(cell_name + "CPW to Microstrip")

        cpw_to_microstrip_points = [(0.0, -2.0), (0.0, 95.0), (589.0, 95.0)]

        cpw_feedline_path = gdspy.FlexPath(
            points=cpw_to_microstrip_points,
            width=2.0,
            offset=0.0,
            corners="circular bend",
            bend_radius=30.0,
            layer=antenna_layer,
        )
        cpw_trench_path = gdspy.FlexPath(
            points=cpw_to_microstrip_points,
            width=13.0,
            offset=0.0,
            corners="circular bend",
            bend_radius=30.0,
            layer=ground_layer,
        )

        # Create bridges in ground plane as the cpw to microstrip transition:
        bridge1 = gdspy.Rectangle(
            (120.0, 101.5),
            (122.0, 88.5),
            layer=ground_layer,
        )
        bridge2 = gdspy.Rectangle(
            (229.5, 101.5),
            (234.0, 88.5),
            layer=ground_layer,
        )
        bridge3 = gdspy.Rectangle(
            (309.5, 101.5),
            (323.9, 88.5),
            layer=ground_layer,
        )
        bridge4 = gdspy.Rectangle(
            (384.8, 101.5),
            (411.1, 88.5),
            layer=ground_layer,
        )
        bridge5 = gdspy.Rectangle(
            (462.8, 101.5),
            (509.3, 88.5),
            layer=ground_layer,
        )
        bridge6 = gdspy.Rectangle(
            (531.4, 101.5),
            (582.2, 88.5),
            layer=ground_layer,
        )
        bridges = [bridge1, bridge2, bridge3, bridge4, bridge5, bridge6]
        # Remove bridges from cpw_trench_path.
        cpw_trench_path = gdspy.boolean(
            cpw_trench_path, bridges, "not", layer=ground_layer
        )

        cpw_to_microstrip_cell.add([cpw_feedline_path, cpw_trench_path])

        # Add cpw to microstrip transition and probes to probes cell.
        cpw_to_microstrip1 = gdspy.CellReference(
            cpw_to_microstrip_cell, origin=(0.0, radius), rotation=0.0
        )
        cpw_to_microstrip2 = gdspy.CellReference(
            cpw_to_microstrip_cell, origin=(0.0, -radius), x_reflection=True
        )
        probe1 = gdspy.CellReference(
            probe_cell, origin=(center_x, center_y), rotation=0.0
        )
        probe2 = gdspy.CellReference(
            probe_cell, origin=(center_x, center_y), rotation=180.0
        )
        probes.add([probe1, probe2, cpw_to_microstrip1, cpw_to_microstrip2])

        # If to add other pair if dual polarisation is desired:
        if self.dual_polarisation == True:
            probe3 = gdspy.CellReference(
                probe_cell, origin=(center_x, center_y), rotation=90.0
            )
            probe4 = gdspy.CellReference(
                probe_cell, origin=(center_x, center_y), rotation=270.0
            )
            cpw_to_microstrip3 = gdspy.CellReference(
                cpw_to_microstrip_cell, origin=(-radius, 0.0), rotation=90.0
            )
            cpw_to_microstrip4 = gdspy.CellReference(
                cpw_to_microstrip_cell,
                origin=(radius, 0.0),
                rotation=90.0,
                x_reflection=True,
            )
            probes.add([probe3, probe4, cpw_to_microstrip3, cpw_to_microstrip4])

        omt_cell.add(ground_hole)
        omt_cell.add(dielectric_hole)
        omt_cell.add(back_etch_hole)
        omt_cell.add(probes)

        return omt_cell

    def get_feedline_connections(self) -> list:
        """
        Function to return the x and y coordinates of the connection points to
        each antenna. The coordinates are returned in the format
        [(x1,y1), (x2,y2)...] to a maximum of four different connection points.
        For the non-rotated OMT the order is as follows:\n
        1: 12 O'clock\n
        2: 6  O'clock\n
        3: 3 O'clock\n
        4: 9 O'clock\n
        """

        radius = self.diameter / 2
        connections = [
            (589.0, radius + 95.0),
            (589.0, -radius - 95.0),
        ]

        if self.dual_polarisation == True:
            connections.append((radius + 95.0, 589.0))
            connections.append((-radius - 95.0, 589.0))

        return connections
