import gdspy
import numpy as np
import math
from models.input_coupling_capacitor import InputCouplingCapacitor
from models.output_coupling_capacitor import OutputCouplingCapacitor
from models.resonator import Resonator


class SingleFilter:
    def __init__(
        self, target_f0: float, feedline_connection_x=0.0, feedline_connection_y=0.0
    ) -> None:
        """
        Creates a new instance of a single filterbank filter, using the modules
        for the resonator, input and output coupling capacitors.

        param: target_f0: Target resonant frequency in GHz for the filter.
        param: input_connection_x: x-coordinate of the connection to the
        feedline.
        param: input_connection_y: y-coordinate of the connection to the
        feedline.
        """

        self.targetF0 = target_f0
        self.feedline_connection_x = feedline_connection_x
        self.feedline_connection_y = feedline_connection_y

        # Define geometry variables from target_f0.
        resW = (
            -5.69e-5 * target_f0**3 + 0.033 * target_f0**2 - 7.13 * target_f0 + 669
        )

        self.res_length = 12 + 2 * resW
        self.cap_in = 0.14 * resW - 10.0
        self.cap_out = 0.121 * resW - 10.7

        # Create instances of sub-structures:
        self.input_coupler = InputCouplingCapacitor(
            cap_in=self.cap_in,
            feedline_connection_x=self.feedline_connection_x,
            feedline_connection_y=self.feedline_connection_y,
        )

        self.resonator = Resonator(
            res_length=self.res_length,
            width=2.0,
            input_connection_x=self.input_coupler.get_resonator_connection_x(),
            input_connection_y=self.input_coupler.get_resonator_connection_y(),
        )

        self.output_coupler = OutputCouplingCapacitor(
            cap_out=self.cap_out,
            resonator_connection_x=self.resonator.get_output_connection_x(),
            resonator_connection_y=self.resonator.get_output_connection_y(),
        )

    def make_cell(
        self, microstrip_layer: int, ground_layer: int, cell_name="Single Filter"
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given filterbank filter geometry.
        The geometry is oriented such that coordinates of the connection to the
        feedline has the largest y value and the connection to the Lekid
        the lowest.

        :param microstrip_layer: GDSII layer for microstirp geometry.
        :param ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param cell_name: Name to be used to reference the cell. If using
        multiple cells of the same type, add an underscore to the cell_name
        followed by a number. e.g. "Single Filter_11"
        """

        # Create Lekid cell to add module cells to
        single_filter_cell = gdspy.Cell(cell_name)

        if "Single Filter" in cell_name:
            filter_number = cell_name.split("_")[1]
            resonator_cell_name = cell_name + " Resonator " + filter_number
            input_coupler_cell_name = cell_name + " Input Coupler " + filter_number
            output_coupler_cell_name = cell_name + " Output Coupler" + filter_number
        else:
            resonator_cell_name = "Resonator"
            input_coupler_cell_name = "Input Coupler"
            output_coupler_cell_name = "Output Coupler"

        # Make gdspy cells:
        resonator_cell = self.resonator.make_cell(
            layer=microstrip_layer, cell_name=resonator_cell_name
        )
        input_coupler_cell = self.input_coupler.make_cell(
            microstrip_layer=microstrip_layer,
            ground_layer=ground_layer,
            cell_name=input_coupler_cell_name,
        )
        output_coupler_cell = self.output_coupler.make_cell(
            microstrip_layer=microstrip_layer,
            ground_layer=ground_layer,
            cell_name=output_coupler_cell_name,
        )

        # Create reference cells of each sub-structure.
        input_coupler_ref_cell = gdspy.CellReference(input_coupler_cell, (0, 0))
        resonator_ref_cell = gdspy.CellReference(resonator_cell, (0, 0))
        output_coupler_ref_cell = gdspy.CellReference(output_coupler_cell, (0, 0))

        # Add reference cells to single_filter_cell.
        single_filter_cell.add(
            [input_coupler_ref_cell, resonator_ref_cell, output_coupler_ref_cell]
        )

        return single_filter_cell

    def get_LEKID_connection_x(self) -> float:
        """
        Function to return the x-coordinate of the connection point to the
        inductor section of the Lekid. NB: This will always be the same as the
        feedline connection x-coordinate
        """
        return self.feedline_connection_x

    def get_LEKID_connection_y(self) -> float:
        """
        Function to return the y-coordinate of the connection point to the
        inductor section of the Lekid.
        """
        return self.output_coupler.get_LEKID_connection_y()
