import gdspy
import numpy as np


class DCResistanceTest:
    def __init__(
        self,
        microstrip_width: float,
        microstrip_length: float,
        meander_width: float,
        origin=None,
    ) -> None:

        """
        Creates an instance of a microstrip DC resistance test structure. A meandering line from left to right.

        :param microstrip_width: Width of microstrip (um).
        :param microstrip_length: Length of the test structure (um).
        :param meander_width: Width of the meander. The distance from the center of the line on a peak to the center of
        the line on a trough.
        :param origin: Coordinates of the left port (x, y)
        """

        if origin is None:
            origin = [0.0, 0.0]
        self.microstrip_width = microstrip_width
        self.microstrip_length = microstrip_length
        self.meander_width = meander_width
        self.origin = origin
        self.midpoint = self.get_midpoint_coordinate()

    def make_cell(
        self,
        microstrip_layer: int,
        cell_name="DC Resistance Test",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given DCResistanceTest instance.

        :param: microstrip_layer: GDSII layer for the microstrip.
        :param: cell_name: Name to be used to reference the cell.
        """

        origin = self.origin

        path_cell = gdspy.Cell(cell_name)

        # Using length of corner from https://www.microwaves101.com/encyclopedias/mitered-bends
        corner_radius = self.microstrip_width * 5
        corner_length = ((np.pi / 2) * corner_radius) - (
            np.pi * self.microstrip_width / 4
        )
        segment_length = (
            2 * corner_length
            + (self.meander_width / 2 - 2 * corner_radius)
            + 2 * (self.meander_width / 2 - corner_radius)
        )

        # Find remaining length after as many segments as possible have been added to complete the full length.
        meander_length = (
            self.microstrip_length
            - (self.microstrip_length % segment_length)
            - 2 * corner_radius
        )
        port_length = (
            (self.microstrip_length - meander_length) / 2
            - corner_length
            + corner_radius
        )

        meander_start = (origin[0] + port_length, 0)

        # Create hole in ground plane:
        microstrip_points = [
            origin,
            meander_start,
        ]

        total_length = 0.0
        positive_start = True
        current_coordinate = meander_start
        while total_length < self.microstrip_length:
            # If there's enough length to add another segment:
            if self.microstrip_length - total_length > segment_length:
                if positive_start:
                    dy = self.meander_width / 2
                    positive_start = False

                else:
                    dy = -self.meander_width / 2
                    positive_start = True

                microstrip_points.append(
                    (current_coordinate[0], current_coordinate[1] + dy)
                )
                microstrip_points.append(
                    (
                        current_coordinate[0] + self.meander_width / 2,
                        current_coordinate[1] + dy,
                    )
                )
                microstrip_points.append(
                    (current_coordinate[0] + self.meander_width / 2, origin[1])
                )
                current_coordinate = (
                    current_coordinate[0] + self.meander_width / 2,
                    origin[1],
                )
                total_length += segment_length

            else:
                microstrip_points.append(
                    (current_coordinate[0] + port_length, origin[1])
                )
                total_length += port_length

        microstrip_path = gdspy.FlexPath(
            points=microstrip_points,
            width=self.microstrip_width,
            offset=0.0,
            corners="circular bend",
            bend_radius=self.microstrip_width * 5,
            max_points=1999,
            layer=microstrip_layer,
        )

        path_cell.add(microstrip_path)

        return path_cell

    def get_output_coordinate(self) -> list:
        """
        Function to return the coordinate of the output port.

        :return:  output port coordinate (x, y).
        """

        # Using length of corner from https://www.microwaves101.com/encyclopedias/mitered-bends
        corner_radius = self.microstrip_width * 5
        corner_length = ((np.pi / 2) * corner_radius) - (
            np.pi * self.microstrip_width / 4
        )
        segment_length = (
            2 * corner_length
            + (self.meander_width / 2 - 2 * corner_radius)
            + 2 * (self.meander_width / 2 - corner_radius)
        )

        # Find remaining length after as many segments as possible have been added to complete the full length.
        meander_length = (
            self.microstrip_length
            - (self.microstrip_length % segment_length)
            - 2 * corner_radius
        )
        port_length = (
            (self.microstrip_length - meander_length) / 2
            - corner_length
            + corner_radius
        )

        number_of_segments = np.floor(self.microstrip_length / segment_length)

        return [
            self.origin[0]
            + 2 * port_length
            + number_of_segments * self.meander_width / 2,
            self.origin[1],
        ]

    def get_midpoint_coordinate(self) -> list:
        """
        Function to return the coordinate of the middle of the test structure.

        :return:  output port coordinate (x, y).
        """

        # Using length of corner from https://www.microwaves101.com/encyclopedias/mitered-bends
        corner_radius = self.microstrip_width * 5
        corner_length = ((np.pi / 2) * corner_radius) - (
            np.pi * self.microstrip_width / 4
        )
        segment_length = (
            2 * corner_length
            + (self.meander_width / 2 - 2 * corner_radius)
            + 2 * (self.meander_width / 2 - corner_radius)
        )

        # Find remaining length after as many segments as possible have been added to complete the full length.
        meander_length = (
            self.microstrip_length
            - (self.microstrip_length % segment_length)
            - 2 * corner_radius
        )
        port_length = (
            (self.microstrip_length - meander_length) / 2
            - corner_length
            + corner_radius
        )

        number_of_segments = np.floor(self.microstrip_length / segment_length)

        return [
            self.origin[0]
            + port_length
            + (number_of_segments * self.meander_width / 2) / 2,
            self.origin[1],
        ]
