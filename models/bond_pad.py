import gdspy
import numpy as np


class BondPad:
    def __init__(
        self,
        cpw_width: float,
        cpw_gap: float,
        axis_of_symmetry=0.0,
        chip_edge=0.0,
    ) -> None:

        """
        Creates a new instance of a bond pad.

        :param cpw_width: Width of CPW centre line joining the bond pad.
        :param cpw_width: Gap of CPW centre line joining the bond pad.
        :param axis_of_symmetry: x coordinate for the centre line of
        symmetry for the bond pad.
        :param chip_edge: y coordinate for the edge of the chip.
        """

        self.cpw_width = cpw_width
        self.cpw_gap = cpw_gap
        self.axis_of_symmetry = axis_of_symmetry
        self.chip_edge = chip_edge

    def make_cell(
        self,
        microstrip_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="Bond Pad",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given BongPad instance.

        :param: microstrip_layer: GDSII layer for the microstrip.
        :param: ground_layer: GDSII layer for the ground plane.
        :param: dielectric_layer: GDSII layer for the dielectric.
        :param: cell_name: Name to be used to reference the cell.
        """
        origin_x = self.axis_of_symmetry
        origin_y = self.chip_edge

        # Create bond pad cell to add geometries to:
        bond_pad_cell = gdspy.Cell(cell_name)

        # Create hole in dielectric layer:
        dielectric_hole = gdspy.Rectangle(
            (origin_x - 512.5, origin_y + 87.5),
            (origin_x + 512.5, origin_y + 1112.5),
            dielectric_layer,
        )
        bond_pad_cell.add(dielectric_hole)

        # Create hole in ground plane:
        ground_hole_points = [
            (origin_x - 1125, origin_y),
            (origin_x - 1125, origin_y + 1100),
            (origin_x - self.cpw_gap - self.cpw_width / 2, origin_y + 1600),
            (origin_x + self.cpw_gap + self.cpw_width / 2, origin_y + 1600),
            (origin_x + 1125, origin_y + 1100),
            (origin_x + 1125, origin_y),
        ]
        ground_hole = gdspy.Polygon(ground_hole_points, layer=ground_layer)
        bond_pad_cell.add(ground_hole)

        # Create microstrip bond pad:
        microstrip_bond_pad_points = [
            (origin_x - 500, origin_y + 100),
            (origin_x - 500, origin_y + 1100),
            (origin_x - self.cpw_width / 2, origin_y + 1600),
            (origin_x + self.cpw_width / 2, origin_y + 1600),
            (origin_x + 500, origin_y + 1100),
            (origin_x + 500, origin_y + 100),
        ]
        microstrip_bond_pad = gdspy.Polygon(
            microstrip_bond_pad_points, layer=microstrip_layer
        )
        bond_pad_cell.add(microstrip_bond_pad)

        return bond_pad_cell

    def get_feedline_connection_x(self) -> float:
        """
        Function to return the x coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.axis_of_symmetry

    def get_feedline_connection_y(self) -> float:
        """
        Function to return the y coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.chip_edge + 1600
