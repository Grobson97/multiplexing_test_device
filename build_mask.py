import gdspy
import numpy as np
import matplotlib.pyplot as plt
import util.filter_bank_builder_tools as tools
import util.spectrometer_builders
import util.general_gds_tools as general_gds_tools
from models.filter_bank import FilterBank
from models.bond_pad import BondPad
from models.alignment_cross import AlignmentCross
from models.dc_resistance_test import DCResistanceTest
from models.lekid import Lekid

chip_unique_id = "3-8 spaced"
library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = library.new_cell("Main")


# Define layer dictionary:
layers = {
    "back_etch": 0,
    "niobium_microstrip": 1,
    "aluminium_microstrip": 2,
    "sin": 3,
    "niobium_ground": 4,
    "wafer": 5,
    "Dice": 6,
}

# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0

# Define spectrometer start coordinates
spectrometer_a_origin = (5000, 12000)
spectrometer_b_origin = (5000, 4000)
spectrometer_c_origin = (5000, -4000)
spectrometer_d_origin = (5000, -12000)
sparse_spectrometer_origin = (-16000, 30000)

four_inch_wafer = gdspy.Round(center=(0, 0), radius=50000, layer=layers["wafer"])
main_cell.add(four_inch_wafer)

spectrometer_group_cell = gdspy.Cell("Spectrometer Group")

################################################################################

# Define target frequencies for filter bank filters:
# NB: Final filter is not included in the mask.
# Define readout frequency band:
frequency_min = 120
frequency_max = 180
oversampling = 1.6

number_of_channels = round(oversampling * 300 * np.log(frequency_max / frequency_min))
target_f0_array = tools.create_target_f0_array(
    frequency_min, frequency_max, number_of_channels
)
# Create instance of filter bank using filter_bank module:
filter_bank = FilterBank(
    target_f0_array=target_f0_array,
    feedline_width=feedline_width,
    filter_separation=0.375,
    input_connection_x=0,
    input_connection_y=0,
)

########################################################################################################################

# Section to add LEKID's to filter-banks

print("*************************************************************************")
print("Quad Spectrometer Details:")

# Create 4 different f0 arrays across 500MHz bandwidth, one for each spectrometer
f0_spacing = 500e3

print("\n\nSpectrometer A:")
lekid_f0_array_a = tools.make_quad_mixed_array(
    2.011e9, 2.039e9, 2.067e9, 2.095e9, f0_spacing, len(target_f0_array)
)
print("\nSpectrometer B:")
lekid_f0_array_b = tools.make_quad_mixed_array(
    2.1345e9, 2.1625e9, 2.1905e9, 2.2185e9, f0_spacing, len(target_f0_array)
)
print("\nSpectrometer C:")
lekid_f0_array_c = tools.make_quad_mixed_array(
    2.258e9, 2.286e9, 2.314e9, 2.342e9, f0_spacing, len(target_f0_array)
)
print("\nSpectrometer D:")
lekid_f0_array_d = tools.make_quad_mixed_array(
    2.3815e9, 2.4095e9, 2.4375e9, 2.4655e9, f0_spacing, len(target_f0_array)
)

print("\nNumber of LEKID's per spectrometer: %i" % (len(target_f0_array)))
print("Frequency spacing between KIDS: %0.4e Hz" % f0_spacing)
print(
    "Inter-filter-bank f0 spacing: %0.4e"
    % (np.min(lekid_f0_array_b) - np.max(lekid_f0_array_a))
)
print(
    "LEKID frequencies span from %0.4e Hz to %0.4e Hz\n"
    % (np.min(lekid_f0_array_a), np.max(lekid_f0_array_d))
)

(
    spectrometer_group_cell,
    spectrometer_group_connection_1,
    spectrometer_group_connection_2,
) = util.spectrometer_builders.make_quad_spectrometer_cell(
    spectrometer_a_origin=spectrometer_a_origin,
    spectrometer_b_origin=spectrometer_b_origin,
    spectrometer_c_origin=spectrometer_c_origin,
    spectrometer_d_origin=spectrometer_d_origin,
    lekid_f0_array_a=lekid_f0_array_a,
    lekid_f0_array_b=lekid_f0_array_b,
    lekid_f0_array_c=lekid_f0_array_c,
    lekid_f0_array_d=lekid_f0_array_d,
    filter_bank=filter_bank,
    spectrometer_group_cell=spectrometer_group_cell,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
)
# Save f0's to text files:
np.savetxt("quad_group_A_f0s.txt", X=lekid_f0_array_a, delimiter=", ")
np.savetxt("quad_group_B_f0s.txt", X=lekid_f0_array_b, delimiter=", ")
np.savetxt("quad_group_C_f0s.txt", X=lekid_f0_array_c, delimiter=", ")
np.savetxt("quad_group_D_f0s.txt", X=lekid_f0_array_d, delimiter=", ")

########################################################################################################################

# Section to add bond pads

bond_pad = BondPad(cpw_width=readout_width, cpw_gap=readout_gap)
bond_pad_cell = bond_pad.make_cell(
    microstrip_layer=layers["niobium_microstrip"],
    ground_layer=layers["niobium_ground"],
    dielectric_layer=layers["sin"],
    cell_name="Bond Pad",
)

aluminium_bond_pad_cell = bond_pad.make_cell(
    microstrip_layer=layers["aluminium_microstrip"],
    ground_layer=layers["niobium_ground"],
    dielectric_layer=layers["sin"],
    cell_name="Al Bond Pad",
)

bond_pad_origin_1 = (44000, 10000)
bond_pad_origin_2 = (44000, -10000)

bond_pad_1_cell = gdspy.Cell("Bond Pad 1")
bond_pad_1_cell.add(gdspy.CellReference(bond_pad_cell, bond_pad_origin_1, rotation=90))
bond_pad_2_cell = gdspy.Cell("Bond Pad 2")
bond_pad_2_cell.add(gdspy.CellReference(bond_pad_cell, bond_pad_origin_2, rotation=90))

aluminium_bond_pad_1_cell = gdspy.Cell("Al Bond Pad 1")
aluminium_bond_pad_1_cell.add(
    gdspy.CellReference(aluminium_bond_pad_cell, bond_pad_origin_1, rotation=90)
)
aluminium_bond_pad_2_cell = gdspy.Cell("Al Bond Pad 2")
aluminium_bond_pad_2_cell.add(
    gdspy.CellReference(aluminium_bond_pad_cell, bond_pad_origin_2, rotation=90)
)

main_cell.add(
    [
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=0),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=0),
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=45),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=45),
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=90),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=90),
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=135),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=135),
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=180),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=180),
        gdspy.CellReference(aluminium_bond_pad_1_cell, (0, 0), rotation=225),
        gdspy.CellReference(aluminium_bond_pad_2_cell, (0, 0), rotation=225),
        gdspy.CellReference(bond_pad_1_cell, (0, 2000), rotation=270),
        gdspy.CellReference(bond_pad_2_cell, (0, 2000), rotation=270),
        gdspy.CellReference(bond_pad_1_cell, (0, 0), rotation=315),
        gdspy.CellReference(bond_pad_2_cell, (0, 0), rotation=315),
    ]
)

########################################################################################################################

# Section to add readout line for spectrometer group:

# Get bond pad connections and translate to location on wafer.
bond_pad_connections = tools.rotate_coordinates(
    bond_pad.get_feedline_connection_x(),
    bond_pad.get_feedline_connection_y(),
    angle=np.pi / 2,
)
bond_pad_1_connections = (
    bond_pad_origin_1[0] + bond_pad_connections[0],
    bond_pad_origin_1[1] + bond_pad_connections[1],
)
bond_pad_2_connections = (
    bond_pad_origin_2[0] + bond_pad_connections[0],
    bond_pad_origin_2[1] + bond_pad_connections[1],
)

readout_path_points_1 = [
    bond_pad_1_connections,
    (bond_pad_1_connections[0] - 3000, bond_pad_1_connections[1]),
    (bond_pad_1_connections[0] - 3000, bond_pad_1_connections[1] + 7500),
    (spectrometer_group_connection_1[0] - 1000, bond_pad_1_connections[1] + 7500),
    (spectrometer_group_connection_1[0] - 1000, spectrometer_group_connection_1[1]),
    (
        spectrometer_group_connection_1[0],
        spectrometer_group_connection_1[1],
    ),
]
readout_centre_path_1 = gdspy.FlexPath(
    points=readout_path_points_1,
    width=readout_width,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_microstrip"],
)

readout_gap_path_1 = gdspy.FlexPath(
    points=readout_path_points_1,
    width=readout_width + 2 * readout_gap,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_ground"],
)

readout_path_points_2 = [
    bond_pad_2_connections,
    (bond_pad_2_connections[0] - 3000, bond_pad_2_connections[1]),
    (bond_pad_2_connections[0] - 3000, bond_pad_2_connections[1] - 7500),
    (spectrometer_group_connection_2[0] - 1000, bond_pad_2_connections[1] - 7500),
    (spectrometer_group_connection_2[0] - 1000, spectrometer_group_connection_2[1]),
    (
        spectrometer_group_connection_2[0],
        spectrometer_group_connection_2[1],
    ),
]
readout_centre_path_2 = gdspy.FlexPath(
    points=readout_path_points_2,
    width=readout_width,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_microstrip"],
)

readout_gap_path_2 = gdspy.FlexPath(
    points=readout_path_points_2,
    width=readout_width + 2 * readout_gap,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_ground"],
)

readout_bridges_1 = tools.bridge_builder(
    path_points=readout_path_points_1,
    bridge_width=readout_width + 2 * readout_gap,
    bridge_thickness=6.0,
    bridge_separation=500.0,
    bridge_layer=layers["niobium_ground"],
    bridge_cell_id="1",
)
readout_bridges_2 = tools.bridge_builder(
    path_points=readout_path_points_2,
    bridge_width=readout_width + 2 * readout_gap,
    bridge_thickness=6.0,
    bridge_separation=500.0,
    bridge_layer=layers["niobium_ground"],
    bridge_cell_id="2",
)
readout_gap_path_1 = gdspy.boolean(
    operand1=readout_gap_path_1,
    operand2=readout_bridges_1,
    operation="not",
    layer=layers["niobium_ground"],
)
readout_gap_path_2 = gdspy.boolean(
    operand1=readout_gap_path_2,
    operand2=readout_bridges_2,
    operation="not",
    layer=layers["niobium_ground"],
)

spectrometer_group_cell.add(
    [
        readout_centre_path_1,
        readout_gap_path_1,
        readout_centre_path_2,
        readout_gap_path_2,
    ]
)

########################################################################################################################

# Section to add single sparse spectrometers:

print("*************************************************************************")
print("Sparse Spectrometer Details:\n\n")

sparse_spectrometer_full_cell = gdspy.Cell("Full Sparse Spectrometer")
sparse_spectrometer_cell = gdspy.Cell("Sparse Spectrometer")

sparse_lekid_f0_array = tools.make_quad_mixed_array(
    2.011e9, 2.1345e9, 2.258e9, 2.3815e9, 4 * f0_spacing, len(target_f0_array)
)

print("\nNumber of LEKID's per spectrometer: %i" % (len(target_f0_array)))
print("Frequency spacing between KIDS: %0.4e Hz" % (f0_spacing * 4))
print(
    "LEKID frequencies span from %0.4e Hz to %0.4e Hz\n"
    % (np.min(sparse_lekid_f0_array), np.max(sparse_lekid_f0_array))
)
# Save f0's to text file:
np.savetxt(fname="sparse_lekid_f0s.txt", X=sparse_lekid_f0_array, delimiter=", ")

(
    sparse_spectrometer_cell,
    sparse_readout_connection_1,
    sparse_readout_connection_2,
) = util.spectrometer_builders.make_single_spectrometer_cell(
    lekid_f0_array=sparse_lekid_f0_array,
    filter_bank=filter_bank,
    spectrometer_cell=sparse_spectrometer_cell,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
)

sparse_spectrometer_reference = gdspy.CellReference(
    sparse_spectrometer_cell, sparse_spectrometer_origin
)
sparse_spectrometer_full_cell.add(sparse_spectrometer_reference)

########################################################################################################################

# Section to add Readout line to sparse spectrometer:

bond_pad_connections = tools.rotate_coordinates(
    bond_pad.get_feedline_connection_x(),
    bond_pad.get_feedline_connection_y(),
    angle=np.pi / 2,
)
sparse_bond_pad_1_connections = tools.rotate_coordinates(
    bond_pad_origin_2[0] + bond_pad_connections[0],
    bond_pad_origin_2[1] + bond_pad_connections[1],
    angle=(3 * np.pi) / 4,
)

sparse_bond_pad_2_connections = tools.rotate_coordinates(
    bond_pad_origin_1[0] + bond_pad_connections[0],
    bond_pad_origin_1[1] + bond_pad_connections[1],
    angle=(3 * np.pi) / 4,
)

sparse_readout_path_points_1 = [
    sparse_bond_pad_1_connections,
    (sparse_bond_pad_1_connections[0] + 2000, sparse_bond_pad_1_connections[1] - 2000),
    (
        sparse_bond_pad_1_connections[0] + 2000,
        sparse_spectrometer_origin[1] + sparse_readout_connection_1[1],
    ),
    (
        sparse_spectrometer_origin[0] + sparse_readout_connection_1[0],
        sparse_spectrometer_origin[1] + sparse_readout_connection_1[1],
    ),
]

sparse_readout_centre_path_1 = gdspy.FlexPath(
    points=sparse_readout_path_points_1,
    width=readout_width,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_microstrip"],
)
sparse_readout_gap_path_1 = gdspy.FlexPath(
    points=sparse_readout_path_points_1,
    width=readout_width + 2 * readout_gap,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_ground"],
)

sparse_readout_path_points_2 = [
    sparse_bond_pad_2_connections,
    (sparse_bond_pad_2_connections[0] + 2000, sparse_bond_pad_2_connections[1] - 2000),
    (sparse_bond_pad_1_connections[0] + 2000, sparse_bond_pad_2_connections[1] - 2000),
    (
        sparse_bond_pad_1_connections[0] + 2000,
        sparse_spectrometer_origin[1] + sparse_readout_connection_2[1],
    ),
    (
        sparse_spectrometer_origin[0] + sparse_readout_connection_2[0],
        sparse_spectrometer_origin[1] + sparse_readout_connection_2[1],
    ),
]
sparse_readout_centre_path_2 = gdspy.FlexPath(
    points=sparse_readout_path_points_2,
    width=readout_width,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_microstrip"],
)
sparse_readout_gap_path_2 = gdspy.FlexPath(
    points=sparse_readout_path_points_2,
    width=readout_width + 2 * readout_gap,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=layers["niobium_ground"],
)

sparse_readout_bridges_1 = tools.bridge_builder(
    path_points=sparse_readout_path_points_1,
    bridge_width=readout_width + 2 * readout_gap,
    bridge_thickness=6.0,
    bridge_separation=500.0,
    bridge_layer=layers["niobium_ground"],
    bridge_cell_id="Sparse 1",
)
sparse_readout_bridges_2 = tools.bridge_builder(
    path_points=sparse_readout_path_points_2,
    bridge_width=readout_width + 2 * readout_gap,
    bridge_thickness=6.0,
    bridge_separation=500.0,
    bridge_layer=layers["niobium_ground"],
    bridge_cell_id="Sparse 2",
)
sparse_readout_gap_path_1 = gdspy.boolean(
    operand1=sparse_readout_gap_path_1,
    operand2=sparse_readout_bridges_1,
    operation="not",
    layer=layers["niobium_ground"],
)
sparse_readout_gap_path_2 = gdspy.boolean(
    operand1=sparse_readout_gap_path_2,
    operand2=sparse_readout_bridges_2,
    operation="not",
    layer=layers["niobium_ground"],
)

sparse_spectrometer_full_cell.add(
    [
        sparse_readout_centre_path_1,
        sparse_readout_gap_path_1,
        sparse_readout_centre_path_2,
        sparse_readout_gap_path_2,
    ]
)
main_cell.add(
    [
        gdspy.CellReference(sparse_spectrometer_full_cell, (0.0, 0.0)),
        gdspy.CellReference(sparse_spectrometer_full_cell, (0.0, 0.0), rotation=180),
    ]
)

########################################################################################################################

# Section to add alignment marks:

alignment_centre_1 = (45000, 0)
alignment_centre_2 = (-45000, 0)

for layer in layers:
    alignment_cross = AlignmentCross()
    alignment_cell = alignment_cross.make_cell(
        layer=layers[layer], cell_name=str(layer) + " alignment cross"
    )
    main_cell.add(
        [
            gdspy.CellReference(alignment_cell, alignment_centre_1),
            gdspy.CellReference(alignment_cell, alignment_centre_2),
        ]
    )

########################################################################################################################

# Section to add dicing trace:

wafer_radius = 50000
edge_length = 2 * wafer_radius * np.cos(3 / 8 * np.pi)
distance_from_centre = wafer_radius * np.sin(3 / 8 * np.pi)

dice_line_cell = gdspy.Cell("Dice line")
dice_line = gdspy.Rectangle(
    (-edge_length / 2, 10), (edge_length / 2, -10), layer=layers["Dice"]
)
dice_line_cell.add(dice_line)

dice_x = 0
dice_y = distance_from_centre

for n in range(8):
    dice_line_reference = gdspy.CellReference(
        dice_line_cell, (dice_x, dice_y), rotation=45 * n
    )
    dice_x, dice_y = tools.rotate_coordinates(dice_x, dice_y, (np.pi / 4))
    main_cell.add(dice_line_reference)


########################################################################################################################

# Section to add aluminium DC test structures:

al_dc_test_cell = gdspy.Cell("Al DC Tests Cell")

aluminium_dc_resistance_test = DCResistanceTest(
    microstrip_width=readout_width,
    microstrip_length=40000.0,
    meander_width=1000,
    origin=(0.0, 0.0),
)

aluminium_dc_line_cell = aluminium_dc_resistance_test.make_cell(
    microstrip_layer=layers["aluminium_microstrip"],
    cell_name="Al DC Resistance Test",
)

aluminium_dc_test_cell_reference = gdspy.CellReference(
    aluminium_dc_line_cell,
    (-aluminium_dc_resistance_test.get_midpoint_coordinate()[0], 40000),
    rotation=0.0,
)

# Connect to bond pads
dc_bond_pad_connection_1 = tools.rotate_coordinates(
    bond_pad_origin_1[0] + bond_pad_connections[0],
    bond_pad_origin_1[1] + bond_pad_connections[1],
    angle=np.pi / 2,
)
dc_bond_pad_connection_2 = util.filter_bank_builder_tools.rotate_coordinates(
    bond_pad_origin_2[0] + bond_pad_connections[0],
    bond_pad_origin_2[1] + bond_pad_connections[1],
    np.pi / 2,
)
util.filter_bank_builder_tools.microstrip_connect_points(
    x1=dc_bond_pad_connection_1[0],
    y1=dc_bond_pad_connection_1[1],
    x2=aluminium_dc_resistance_test.origin[0]
    - aluminium_dc_resistance_test.midpoint[0],
    y2=aluminium_dc_resistance_test.origin[1] + 40000,
    microstrip_width=readout_width,
    bend_radius=200,
    microstrip_layer=layers["aluminium_microstrip"],
    target_cell=al_dc_test_cell,
    x_first=False,
)
util.filter_bank_builder_tools.microstrip_connect_points(
    x1=dc_bond_pad_connection_2[0],
    y1=dc_bond_pad_connection_2[1],
    x2=aluminium_dc_resistance_test.get_output_coordinate()[0]
    - aluminium_dc_resistance_test.midpoint[0],
    y2=aluminium_dc_resistance_test.get_output_coordinate()[1] + 40000,
    microstrip_width=readout_width,
    bend_radius=200,
    microstrip_layer=layers["aluminium_microstrip"],
    target_cell=al_dc_test_cell,
    x_first=False,
)

al_dc_test_cell.add([aluminium_dc_test_cell_reference])


########################################################################################################################

# # Section to add niobium DC test structures

nb_dc_test_cell = gdspy.Cell("Nb DC Tests Cell")

niobium_dc_resistance_test = DCResistanceTest(
    microstrip_width=readout_width,
    microstrip_length=40000.0,
    meander_width=1000,
    origin=(0.0, 0.0),
)

niobium_dc_line_cell = niobium_dc_resistance_test.make_cell(
    microstrip_layer=layers["niobium_microstrip"],
    cell_name="Nb DC Resistance Test",
)

niobium_dc_test_cell_reference = gdspy.CellReference(
    niobium_dc_line_cell,
    (-niobium_dc_resistance_test.get_midpoint_coordinate()[0], 36500),
    rotation=0.0,
)

# Connect to bond pads
dc_bond_pad_connection_1 = tools.rotate_coordinates(
    bond_pad_origin_1[0] + bond_pad_connections[0],
    bond_pad_origin_1[1] + bond_pad_connections[1],
    angle=np.pi / 2,
)
dc_bond_pad_connection_2 = util.filter_bank_builder_tools.rotate_coordinates(
    bond_pad_origin_2[0] + bond_pad_connections[0],
    bond_pad_origin_2[1] + bond_pad_connections[1],
    np.pi / 2,
)
util.filter_bank_builder_tools.microstrip_connect_points(
    x1=dc_bond_pad_connection_1[0],
    y1=dc_bond_pad_connection_1[1] - 2000,
    x2=niobium_dc_resistance_test.origin[0]
    - niobium_dc_resistance_test.midpoint[0],
    y2=niobium_dc_resistance_test.origin[1] + 36500,
    microstrip_width=readout_width,
    bend_radius=200,
    microstrip_layer=layers["niobium_microstrip"],
    target_cell=nb_dc_test_cell,
    x_first=False,
)
util.filter_bank_builder_tools.microstrip_connect_points(
    x1=dc_bond_pad_connection_2[0],
    y1=dc_bond_pad_connection_2[1] - 2000,
    x2=niobium_dc_resistance_test.get_output_coordinate()[0]
    - niobium_dc_resistance_test.midpoint[0],
    y2=niobium_dc_resistance_test.get_output_coordinate()[1] + 36500,
    microstrip_width=readout_width,
    bend_radius=200,
    microstrip_layer=layers["niobium_microstrip"],
    target_cell=nb_dc_test_cell,
    x_first=False,
)

nb_dc_test_cell.add([niobium_dc_test_cell_reference])
main_cell.add(
    [
        gdspy.CellReference(al_dc_test_cell, rotation=135),
        gdspy.CellReference(nb_dc_test_cell, rotation=180),
    ]
)

########################################################################################################################

# Section to add feedline test structures

bridgeless_feedline_test_cell = gdspy.Cell("Bridgeless feedline test")
feedline_test_cell = gdspy.Cell("Feedline test")

tools.cpw_connect_points(
    x1=dc_bond_pad_connection_1[0],
    y1=dc_bond_pad_connection_1[1],
    x2=dc_bond_pad_connection_1[0] + 5000,
    y2=dc_bond_pad_connection_1[1] - 3500,
    centre_width=readout_width,
    ground_gap=readout_gap,
    bend_radius=200,
    centre_layer=layers["niobium_microstrip"],
    ground_layer=layers["niobium_ground"],
    target_cell=bridgeless_feedline_test_cell,
    x_first=False,
    add_bridges=False
)
tools.cpw_connect_points(
    x1=dc_bond_pad_connection_1[0] + 5000,
    y1=dc_bond_pad_connection_1[1] - 3500,
    x2=dc_bond_pad_connection_2[0],
    y2=dc_bond_pad_connection_2[1],
    centre_width=readout_width,
    ground_gap=readout_gap,
    bend_radius=200,
    centre_layer=layers["niobium_microstrip"],
    ground_layer=layers["niobium_ground"],
    target_cell=bridgeless_feedline_test_cell,
    x_first=True,
    add_bridges=False
)
tools.cpw_connect_points(
    x1=dc_bond_pad_connection_1[0],
    y1=dc_bond_pad_connection_1[1],
    x2=dc_bond_pad_connection_1[0] + 5000,
    y2=dc_bond_pad_connection_1[1] - 3500,
    centre_width=readout_width,
    ground_gap=readout_gap,
    bend_radius=200,
    centre_layer=layers["niobium_microstrip"],
    ground_layer=layers["niobium_ground"],
    target_cell=feedline_test_cell,
    x_first=False,
    add_bridges=True
)
tools.cpw_connect_points(
    x1=dc_bond_pad_connection_1[0] + 5000,
    y1=dc_bond_pad_connection_1[1] - 3500,
    x2=dc_bond_pad_connection_2[0],
    y2=dc_bond_pad_connection_2[1],
    centre_width=readout_width,
    ground_gap=readout_gap,
    bend_radius=200,
    centre_layer=layers["niobium_microstrip"],
    ground_layer=layers["niobium_ground"],
    target_cell=feedline_test_cell,
    x_first=True,
    add_bridges=True
)

main_cell.add(
    [
        gdspy.CellReference(bridgeless_feedline_test_cell),
        gdspy.CellReference(feedline_test_cell, rotation=315)
    ]
)


########################################################################################################################

# Section to add unique id and logos

chip_unique_id_text = gdspy.Text(
    text=chip_unique_id,
    size=1000,
    position=(-1000 * (len(chip_unique_id) * ((5 / 9) + (11 / 9))) / 4, 43000),
    layer=layers["niobium_ground"],
)

# cardiff_logo_cell = general_gds_tools.import_gds("logos\\cardiff_logo.gds", cell_name="Cardiff Logo")


main_cell.add([
    chip_unique_id_text,
    # gdspy.CellReference(cardiff_logo_cell)
])

########################################################################################################################

spectrometer_group_reference_1 = gdspy.CellReference(
    spectrometer_group_cell, (0, 0), rotation=180
)
spectrometer_group_reference_2 = gdspy.CellReference(
    spectrometer_group_cell, (0, 0), rotation=0
)

main_cell.add([spectrometer_group_reference_1, spectrometer_group_reference_2])

plt.figure(figsize=(8, 6))
for count, f in enumerate(lekid_f0_array_a):
    label_a = ""
    label_b = ""
    label_c = ""
    label_d = ""
    if count == 0:
        label_a = "Spectrometer A"
        label_b = "Spectrometer B"
        label_c = "Spectrometer C"
        label_d = "Spectrometer D"
    plt.vlines(x=f * 1e-9, ymin=-40, ymax=0, colors="r", label=label_a)
    plt.vlines(
        x=lekid_f0_array_b[count] * 1e-9, ymin=-40, ymax=0, colors="g", label=label_b
    )
    plt.vlines(
        x=lekid_f0_array_c[count] * 1e-9, ymin=-40, ymax=0, colors="b", label=label_c
    )
    plt.vlines(
        x=lekid_f0_array_d[count] * 1e-9, ymin=-40, ymax=0, colors="y", label=label_d
    )
plt.xlabel("Frequency (GHz)")
plt.yticks(color="w")
plt.title("Quad Spectrometer f0 Distribution")
plt.legend()
plt.show()

plt.figure(figsize=(8, 6))
for count, f in enumerate(sparse_lekid_f0_array):
    plt.vlines(x=f * 1e-9, ymin=-40, ymax=0, colors="r")
plt.xlabel("Frequency (GHz)")
plt.yticks(color="w")
plt.title("Sparse Spectrometer f0 Distribution")
plt.show()

# gdspy.LayoutViewer(library=library, pattern={"default": 9})
gdspy.write_gds("mask_files\\" + chip_unique_id + "_mask.gds")
