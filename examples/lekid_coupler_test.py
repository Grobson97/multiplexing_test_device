import gdspy
from models.lekid_coupler import LekidCoupler


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_lekid_coupler = LekidCoupler(
        coupler_length=600.0,
        finger_width=3.0,
        finger_gap=8.0,
        readout_track_width=6.0,
        idc_track_width=6.0,
        length_to_idc=30.0,
        length_to_readout=82,
        idc_connection_x=0,
        idc_connection_y=0,
    )

    test_lekid_coupler_cell = test_lekid_coupler.make_cell(
        readout_line_layer=0,
        back_etch_layer=1,
        idc_layer=2,
        ground_layer=3,
        dielectric_layer=4,
        cell_name="Lekid Coupler",
    )

    main_cell.add(test_lekid_coupler_cell)

    print(test_lekid_coupler.get_readout_connection_x())
    print(test_lekid_coupler.get_readout_connection_y())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
