import gdspy
from models.lekid import Lekid


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_lekid = Lekid(
        target_f0=2.0e9,
        signal_input_x=0.0,
        signal_input_y=0.0,
    )

    test_lekid_cell = test_lekid.make_cell(
        back_etch_layer=0,
        inductor_layer=1,
        idc_layer=1,
        readout_layer=2,
        ground_layer=3,
        dielectric_layer=4,
        readout_width=16.0,
        readout_gap=10.0,
        cell_name="Lekid",
    )
    main_cell.add(test_lekid_cell)

    print(test_lekid.get_readout_connections(readout_width=16.0, readout_gap=10.0))
    print(test_lekid.get_readout_connections(readout_width=16.0, readout_gap=10.0)[0])

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
