import gdspy
from models.single_filter import SingleFilter

library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
# Create Lekid cell to add module cells to
main_cell = library.new_cell("Main")

single_filter = SingleFilter(target_f0=150)
single_filter_cell = single_filter.make_cell(
    microstrip_layer=1, ground_layer=0, cell_name="TEST Filter_1"
)
main_cell.add(single_filter_cell)

gdspy.LayoutViewer()
