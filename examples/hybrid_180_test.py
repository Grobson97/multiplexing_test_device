import gdspy
from models.hybrid_180 import Hybrid180


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_hybrid_180 = Hybrid180(
        resonator_length=640,
        loop_width=2.0,
    )

    test_hybrid_180_cell = test_hybrid_180.make_cell(
        microstrip_layer=0, cell_name="Hybrid Test"
    )

    main_cell.add(test_hybrid_180_cell)

    print(test_hybrid_180.get_input_connection_A())
    print(test_hybrid_180.get_input_connection_B())
    print(test_hybrid_180.get_difference_port_connection())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
