import gdspy
from models.resonator import Resonator


def main():

    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_resonator = Resonator(
        res_length=200, width=2, input_connection_x=0.0, input_connection_y=0.0
    )
    resonator_cell = test_resonator.make_cell(layer=0)
    main_cell.add(resonator_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
