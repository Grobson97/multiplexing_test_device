import gdspy
import numpy as np
from models.filter_bank import FilterBank

library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
# Create Lekid cell to add module cells to
main_cell = library.new_cell("Main")

target_f0_array = np.array(
    [180.0, 173.5, 167.2, 161.2, 155.3, 149.7, 144.3, 139.1, 134.0, 129.2, 124.5, 120.0]
)

filter_bank = FilterBank(
    target_f0_array=target_f0_array,
    feedline_width=2.0,
    filter_separation="3/4",
    input_connection_x=0,
    input_connection_y=0,
)

filter_bank_cell = filter_bank.make_cell(microstrip_layer=1, ground_layer=0)
main_cell.add(filter_bank_cell)

print(filter_bank.get_lekid_connections())

gdspy.LayoutViewer()
