# Multiplexing Test Device

## Build mask:
1. Install the required packages shown in requirements.txt.
2. Run the build_mask.py script to build the gds mask for the SPT-SLIM multiplexing test chip. This will output print 
statements of the details for the quad spectrometer f0 array distribution and also the sparse spectrometers. Two plots
will also be produced showing these details graphically. Finally the gds file will be saved under the name
"SPT_SLIM_multiplexing_test_mask.gds".
3. Open the gds file, preferably in K Layout and load the "layer_properties.lyp" file. Note the layer colours were
chosen using K layout in dark mode.

## Model examples:

You can also run scripts to build and view a single component by running one of the test scripts in the "examples"
directory.
